<?php
 
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Vườn thông minh</title>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" type="image/x-icon" href="http://iot.dientuminhviet.com/image/favicon.ico">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script type="text/javascript">

setInterval(function()
{
    $(document).ready(function(){ 
        // lấy dữ liệu từ file loaddata.php hiển thị lại lên màn hình cứ sau mỗi 30s 
    $.ajax({
        url:"loaddata.php",
        type:"post",
        data:"",
        async:false,
        success:function(kq)
        {
         var dulieu = $.parseJSON(kq);
         $("#nd").html(dulieu.nd);
         $("#da").html(dulieu.da);
         $("#q").html(dulieu.q);
         $("#ps").html(dulieu.ps);
        }
    })  

    });  
}, 30000);

$(document).ready(function(){ 
    // lấy dữ liệu từ file loaddata.php hiển thị lại lên màn hình lần đầu tiên 
    $.ajax({
        url:"loaddata.php",
        type:"post",
        data:"",
        async:false,
        success:function(kq)
        {
         var dulieu = $.parseJSON(kq);
         $("#nd").html(dulieu.nd);
         $("#da").html(dulieu.da);
         $("#q").html(dulieu.q);
         $("#ps").html(dulieu.ps);
        }
    })  

    });
    
</script>
</head>

<body>
<div id="dautrang">
      <h2 style="color: #FFF;text-align:center;margin: auto;font-size: 30px;background-color: #18aa8d;padding-bottom: 25px;padding-top: 20px ">Nhà vườn thông minh - Đồ án </h2>
</div>
<div id="thantrang">
    <div id="nhietdo">
       <div style="padding-left: 40px">
        <img src="https://goo.gl/CEHv6C" width="45" height="auto" style="float: left;"> 
         <p style="font-size: 40px;padding-left: 145px;padding-top: 10px;color: #FFF">Nhiệt độ</p>
         <?php echo "<p id='nd' style='font-size: 90px;color: #FFF;padding-left: 145px'></p>"; ?>
        </div>
    </div>
    <div id="c1"></div>
    <div class="img_a" id="doam">
        <div style="padding-left: 5px">
        <img src="https://goo.gl/RnQCHP" width="120" height="auto" style="float: left;" style="padding-top: 40px"> 
         <p style="font-size: 40px;padding-left: 160px;padding-top: 10px;color: #FFF">Độ ẩm</p>
         <?php echo "<p id='da' style='font-size: 90px;color: #FFF;padding-left: 160px'></p>"; ?>
        </div>
    </div>
    <div id="c1"></div>
     <div id="lichsu">     
        <div style="height: 45px; background-color: #c54141;font-size: 30px;color: #FFF;padding-left: 10px; padding-top: 10px"> 
            <p style="float: left;">Biểu đồ nhiệt</p> 
            <a style="padding-left: 190px;" href="https://thingspeak.com/channels/453996/feed.csv?timezone=Asia%2FBangkok"><img src="http://sv1.upsieutoc.com/2018/03/20/jrafectexyjeursvcovx.png" width="40" height="40" style="padding-bottom: 10px;"></a>
        </div>
        <div id="nhiet">
           <iframe width="450" height="260" style="border: 3px solid #cccccc;" src="https://thingspeak.com/channels/453996/charts/1?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=60&type=line&update=15"></iframe>
        </div>

        <div id="c1"></div>
        <div style="height: 45px; background-color: #2F7EB2;font-size: 30px;color: #FFF;padding-left: 10px; padding-top: 10px"> 
            <p style="float: left;">Biểu đồ độ ẩm</p> 
            <a style="padding-left: 190px;" href="https://thingspeak.com/channels/454021/feed.csv?timezone=Asia%2FBangkok"><img src="http://sv1.upsieutoc.com/2018/03/20/jrafectexyjeursvcovx.png" width="40" height="40" style="padding-bottom: 10px;"></a>

        </div>
        <iframe width="450" height="260" style="border: 3px solid #cccccc;" src="https://thingspeak.com/channels/454021/charts/1?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=60&type=line&update=15"></iframe>
         <div id="c1"></div>

         <table class="MyTable" align="center" border="0" cellpadding="5" cellspacing="1" style="padding-top: 10px">       
       <thead>
        <tr>
            <th>Tên Thiết bị</th>
            <th>Hình ảnh</th>
            <th>Trạng thái</th>
        </tr>   
        <tbody>
            <tr>
              <td>Quạt phun sương</td>
              <td><img src="http://sv1.upsieutoc.com/2018/03/20/fan-icon-12.png" width="50" height="50"></td>
              <td><img src="http://sv1.upsieutoc.com/2018/03/14/b888f8f00ea5d9f05.png"></td>
            </tr>
            <tr>
              <td>Đèn</td>
              <td><img src="http://sv1.upsieutoc.com/2018/03/20/images.png" width="50" height="50"></td>
              <td><img src="http://sv1.upsieutoc.com/2018/03/14/t4dc3cac023a0cae7.png"></td>
            </tr>
        </tbody>
       </thead>
     </table>   
    <div id="c1"></div>
    </div>
</div>
    
     <h2 style="color: #FFF;text-align:center;margin: auto;font-size: 30px;background-color: #18aa8d;padding-bottom: 25px;padding-top: 20px;min-width: 456;"> Hệ thống giám sát Nhiệt độ - Độ ẩm - Tự động điều khiển đèn, phun sương </h2>
 
</body>
</html>